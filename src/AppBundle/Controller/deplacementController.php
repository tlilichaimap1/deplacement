<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\deplacement;
use AppBundle\Entity\ville;


class deplacementController extends Controller {
/**
* @Route("/create-deplacement")
*/
public function createAction(Request $request) {
  $deplacement = new deplacement();
  
  $form = $this->createFormBuilder($deplacement)
   
    ->add('nom', TextType::class)
    ->add('prenom', TextType::class)
    ->add('email', TextType::class)
    ->add('Datedepart', DateType::class)
    ->add('dateretour', DateType::class)
    ->add('heuredepart', TimeType::class)
    ->add('heureretour', TimeType::class)
 
 ->add('moyentransport',ChoiceType::class,array(
                'choices'  => array(
                    'avion' => "avion",
                    'voiture' => "voiture",
                    'metro' => "métro",
                ),
                'expanded' => true,
                'multiple' => false
            ))
            ->add('idville',ChoiceType::class,array(
                'choices'  => array(
                    'tunis' => "1",
                    'marcoo' => "2",
                    'paris' => "3",
                ),
                
            ))
    ->add('save', SubmitType::class, array('label' => 'New deplacement'))    
    ->add('annuler', SubmitType::class, array('label' => 'annuler'))
    ->getForm();

  $form->handleRequest($request);

  if ($form->isSubmitted()) {

    $dep = $form->getData();

    $em = $this->getDoctrine()->getManager();
    $em->persist($dep);
    $em->flush();

    return $this->redirect('/view-deplacement');

  }

  return $this->render(
    'deplacement/ajout.html.twig',
    array('form' => $form->createView())
    );

}
/**
* @Route("/view-deplacement")
*/  
public function showAction() {

  $deplacements = $this->getDoctrine()
    ->getRepository('AppBundle:deplacement')
    ->findAll();

  return $this->render(
    'deplacement/show.html.twig',
      array('deplacements' => $deplacements)
    );

}

/**
* @Route("/delete-deplacement/{id}")
*/ 
public function deleteAction($id) {

  $em = $this->getDoctrine()->getManager();
  $deplacement = $em->getRepository('AppBundle:deplacement')->find($id);

  if (!$deplacement) {
    throw $this->createNotFoundException(
    'There are no articles with the following id: ' . $id
    );
  }

  $em->remove($deplacement);
  $em->flush();

  return $this->redirect('/view-deplacement');

}

/**
 * @Route("/update-deplacement/{id}")
 */
public function updateAction(Request $request, $id) {
    
    $em = $this->getDoctrine()->getManager();
    $deplacement = $em->getRepository('AppBundle:deplacement')->find($id);
    
    if (!$deplacement) {
        throw $this->createNotFoundException(
            'There are no deplacement with the following id: ' . $id
            );
    }
    
    $form = $this->createFormBuilder($deplacement)
    ->add('nom', TextType::class)
    ->add('prenom', TextType::class)
    ->add('email', TextType::class)
    ->add('Datedepart', DateType::class)
    ->add('dateretour', DateType::class)
    ->add('heuredepart', TimeType::class)
    ->add('heureretour', TimeType::class)
    
    ->add('moyentransport',ChoiceType::class,array(
        'choices'  => array(
            'avion' => "avion",
            'voiture' => "voiture",
            'metro' => "métro",
        ),
        'expanded' => true,
        'multiple' => false
    ))
    ->add('idville',ChoiceType::class,array(
        'choices'  => array(
            'tunis' => "1",
            'marcoo' => "2",
            'paris' => "3",
        ),
        
    ))
        ->add('save', SubmitType::class, array('label' => 'Update'))
        ->add('annuler', SubmitType::class, array('label' => 'annuler'))
        ->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            
            $deplacement = $form->getData();
            $em->flush();
            
            return $this->redirect('/view-deplacement');
            
        }
        
        return $this->render(
            'deplacement/ajout.html.twig',
            array('form' => $form->createView())
            );
        
}





}
?>
